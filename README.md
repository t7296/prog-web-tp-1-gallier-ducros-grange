# TP Programmation Web - Données Ouvertes et Multimédia

## Participants 

* Gallier Loric
* Ducros Vincent
* Grange Mathis


Pour le TP1 voir [ici](./Exercices/TP1/README.md).

Pour le TP2 voir [ici](./Exercices/TP2/README.md).

Pour le TP3 voir [ici](./Exercices/TP3/README.md).