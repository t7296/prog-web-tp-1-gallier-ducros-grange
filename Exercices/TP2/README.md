% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : Bornes WiFi

## Participants 

* Gallier Loric
* Ducros Vincent
* Grange Mathis

## Filtres Unix
### Question 1 - Comptage

On a enlevé l'entête pour simplifier la lecture du fichier.

La commande pour connaître le nombre de bornes WiFi dans le fichier CSV est la suivante :
`wc -l <fichier_bornes_wifi.csv>`

    On obtient 68 bornes au total. 

### Question 2 - Points multiples

Ce fichier CSV est assez difficile à parser pour plusieurs raisons. Les lieux semblables ne possèdent pas toujours les mêmes informations, rendant difficile l'élimination des doublons. De plus, le délimiteur utilisé, la virgule, est utilisé à l'intérieur même des champs.
Cependant on peut filtrer un certain nombres de bornes en utilisant la commande suivante :

`cut -d ',' -f 2 borneswifi_EPSG4326.csv | uniq -c`
Il y a ainsi un maximum de 5 bornes au même endroit, à savoir la Bibliothèque Etudes. L'Hôtel de ville a plus de bornes que ça, mais certaines de ses bornes ont des emplacements différents selon les données du fichier.

On rajoute `| wc -l` pour calculer le nombre d'emplacements uniques

qui renvoie 58 emplacements différents. 

## Traitements PHP

### Question 3 & 4 - Comptage PHP et Structure de données

Voir [comptage.php](./comptage.php). En affichant la variable on obtiens un tableau de la forme :

```
Array
(
    [1] => Array
        (
            [name] => AP_ANTENNE2
            [adr] => Antenne 2
            [lat] => 5.727524
            [lon] => 45.192460
        )

    [2] => Array
        (
            [name] => AP_ANTENNE3
            [adr] => Antenne 3
            [lat] => 5.707345
            [lon] => 45.172551
        )

    [3] => Array
        (
            [name] => AP_ANTENNE4
            [adr] => Antenne 4
            [lat] => 5.725943
            [lon] => 45.177295
        )
etc...
```

### Question 5 - Proximité

Voir [proximite_grenette.php](./proximite_grenette.php)

On utilise la commande 'php proximite_grenette.php | sort -n' pour trier les bornes par distance croissante et 'php proximite_grenette.php | wc -l' pour avoir le nombre de bornes dans les 200m de la place Grenette.

La plus proche est à 20,3m et il y en a 4 dans les 200m autour des coordonnées choisies.

### Question 6 - Proximité top N

Voir [proximite_n.php](./proximite_n.php)

### Question 7 - Géocodage

Voir [fct_borneswifi.php](./fct_borneswifi.php)  et [main.php](./main.php)
où l'on a également refactorisé le code en fonctions utilisables pour les questions suivantes.
 
### Question 8 - Webservice

Voir [webservice_q8.php](./webservice_q8.php) 


### Question 9 - Format JSON

Voir [comptagejson.php](./comptagejson.php)

### Question 10 - Client webservice

Voir [formulaire.php](./formulaire.php)

## Antennes GSM

### Question 1 - CSV Antennes

Il y a 101 antennes GSM référencées à Grenoble.
Les différents champs de données dans le fichier csv sont :

ANT_ID : L'identifiant de l'antenne
ADRES_ID :  Information technique sur l'antenne
MICROCELL : OUI/NON Information technique sur l'antenne
OPERATEUR : L'opérateur de l'antenne
ANT_TECHNO : Listes des technologies de connection de l'antenne
X;Y;ANT_ADRES_LIBEL : Des coordonnées et l'adresse de l'antenneca+
NUM_CARTORADIO;NUM_SUPPORT;ANT_2G;ANT_3G;ANT_4G : Et d'autres informations techniques sur l'antenne

Ce fichier contient l'adresse de l'antenne en entier, ce qui représente un avantage par rapport au jeu de données précédent qui manquait de précision sur ce champ.

### Question 2 - Statistiques opérateurs

On obtient les informations suivantes avec cette commande : `cut -d ';' -f 4 Antennes_GSM_Grenoble.csv | sort | uniq -c`
Il y a 4 opérateurs différents :
* Bouygues Telecom (BYG) qui possède 26 antennes
* Free (FREE) qui possède 18 antennes
* Orange (ORA) qui possède 26 antennes
* SFR (SFR) qui possède 30 antennes

### Question 3 - KML validation

Ce fichier ne peut pas être validé. Il comporte des champs non standards, et rien ne nous est fourni pour valider un fichier comportant ces ajouts.

### Question 4 - KML bis

Ce fichier n'est pas simple à lire visuellement, toutefois il est efficace lorsque l'on se sert de bibliothèques prévues à cet effet.
Cependant, ce format est très peu compact et assez redondant (Champs répétés à chaque donnée).

### Question 5 - Top N opérateur

On a repris un code similaire à celui utilisé pour la question 1.10 avec en entrée le fichier `Antennes_GSM_Grenoble.json`.

Voir [fct_antennes.php](./fct_antesnnes.php)  et [formulaire_cellulaire.php](./formulaire_cellulaire.php).









