<?php require_once("fct_antennes.php");?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="formulaire.css" />
        <title>Données ouvertes Grenoble</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </head>
    <body>

    <nav>
        <ul>
            <li><a href="formulaire.php">Bornes Wifi</a></li>
            <li><a href="formulaire_cellulaire.php">Antennes cellulaires</a></li>
        </ul>
    </nav>
    
        <div class="formulaire">
            <p> Rentrer des cordonnées gps et trouver les N Antennes cellulaire de l'opérateur les plus proches de ce point</p>
            <form method="get" action="formulaire_cellulaire.php">
                <ul>
                    <label for="Longitude">longitude</label> 
                    <input type="number" step="any" id="lon" name="lon" value="5.724524"/> <br /> <br />

                    <label for="Latitude">latitude</label> 
                    <input type="number" step="any" id="lat" name="lat" value="45.188529"/> <br /> <br />

                    <label for="top">Nombre de points</label>            
                    <input type="number" step="1" min="1" id="top" name="top" value="10"/> <br /> <br />

                    <label for="operateur">Opérateur</label>   
                    <select id="operateur" name="operateur">
                        <option value="BYG">Bouygues</option>
                        <option value="FREE">Free</option>
                        <option value="ORA">Orange</option>
                        <option value="SFR">Sfr</option>
                    </select> <br />
                </ul> <br />
                <input type="submit" value="Trouver points" />
            </form>
        </div>
        <?php if (isset($_GET['top']) && isset($_GET['lon']) && isset($_GET['lat']) ) {
            echo '<div class="table">';
            to_HTML_antennes( geopoint($_GET['lon'],$_GET['lat']), $_GET['top'], $_GET['operateur'], antennes_as_array("Antennes_GSM_Grenoble.json") );
            echo '</div>';
        }
     ?>
    </body>
</html>