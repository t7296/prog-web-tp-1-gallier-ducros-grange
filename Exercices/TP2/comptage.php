<?php
require_once("../../Helpers/tp2-helpers.php");

    //$stream=fopen("borneswifi_EPSG4326.csv", "r");
    //$array = fgetcsv($stream);
    $array_tmp=file("borneswifi_EPSG4326.csv");
    $nb_bornes=count($array_tmp)-1; //la premiere ligne est un entete
    echo "Nb bornes : $nb_bornes\n";
    $tableau_associatif;
    for($i=1; $i<$nb_bornes; $i++) {
        $array=str_getcsv($array_tmp[$i]);
        $tableau_associatif[$i] = [
            'name' => $array[0],
            'adr' => $array[1],
            'lat' => $array[2],
            'lon' => $array[3],
        ];
    }
    print_r($tableau_associatif);
?>