<?php
require_once("../../Helpers/tp2-helpers.php");

    //$stream=fopen("borneswifi_EPSG4326.csv", "r");
    //$array = fgetcsv($stream);
    $array_tmp=file("borneswifi_EPSG4326.csv");
    $tableau_associatif;
    for($i=1; $i<count($array_tmp); $i++) {
        $array=str_getcsv($array_tmp[$i]);
        $tableau_associatif[$i] = [
            'name' => $array[0],
            'adr' => $array[1],
            'lat' => $array[2],
            'lon' => $array[3],
        ];
    }
    $j=0;
    foreach($tableau_associatif as $borne ) {
        $coords_grenette = ['lon' => '45.19102', 'lat' => '5.72752'];
        $coords_borne = ['lon' => $borne['lon'], 'lat' => $borne['lat']];
        $distance = distance($coords_grenette,$coords_borne);
        $tableau_bornes_distance[$j] = [ 'name' => $borne['name'] , 'dist' => $distance ];
        $j++;
    }
    $array_distance = array_column($tableau_bornes_distance, 'dist');
    array_multisort($array_distance, SORT_NUMERIC, $tableau_bornes_distance);
    if (isset($argv[1])) {
        for($j=0; $j<$argv[1]; $j++) {
            $name_borne = $tableau_bornes_distance[$j]['name'];
            $distance = $tableau_bornes_distance[$j]['dist'];
            echo "$name_borne , $distance\n";
        }
    } else {
        echo "Rentrez un parametre N (bornes proches de la place Grenette\n";
    }
?>