<?php
require_once("../../Helpers/tp2-helpers.php");

    //$stream=fopen("borneswifi_EPSG4326.csv", "r");
    //$array = fgetcsv($stream);
    $array_tmp=file("borneswifi_EPSG4326.csv");
    $tableau_associatif;
    for($i=1; $i<count($array_tmp); $i++) {
        $array=str_getcsv($array_tmp[$i]);
        $tableau_associatif[$i] = [
            'name' => $array[0],
            'adr' => $array[1],
            'lat' => $array[2],
            'lon' => $array[3],
        ];
    }
    foreach($tableau_associatif as $borne ) {
        $coords_grenette = ['lon' => '45.19102', 'lat' => '5.72752'];
        $coords_borne = ['lon' => $borne['lon'], 'lat' => $borne['lat']];
        $distance = distance($coords_grenette,$coords_borne);
        $name_borne = $borne['name'];
        if($distance<200) // a enlever pr afficher toutes les bornes
            echo "$distance --> Distance de la borne $name_borne\n";

    }
?>