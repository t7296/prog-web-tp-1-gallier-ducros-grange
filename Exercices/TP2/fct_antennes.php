<?php
require_once("../../Helpers/tp2-helpers.php");

    //Renvoie un tableau de tableau associatif avec nom, adr, lat, lon
    function antennes_as_array($filename) {
        $array_tmp=json_decode(file_get_contents($filename),true);

        $tableau_associatif;
        $i=0;
        foreach($array_tmp['features'] as $antenne) {
            //cree une structure lisible en php
            $tableau_associatif[$i] = [
                'operateur' => $antenne['properties']['OPERATEUR'],
                'location' => $antenne['properties']['ANT_ADRES_LIBEL'],
                '4g' => $antenne['properties']['ANT_4G'],                
                'lon' => $antenne['geometry']['coordinates']['0'],
                'lat' => $antenne['geometry']['coordinates']['1'],
            ];
            $i++;
        }

        //print_r($tableau_associatif); decommentez pour afficher le tableau 
        return $tableau_associatif;
    }

    function tab_N_dist_antennes($coords,$N,$operateur,$tableau_associatif) {
        $j=0;
        $new_tableu_associatif;
        foreach($tableau_associatif as $antenne ) {
            if ($antenne['operateur']==$operateur) { //restreint a 1 operateur
                $new_tableau_associatif[$j] = $antenne; //on remet ttes les infos ds le nouveau tableau

                //calcul distance
                $coords_antenne = geopoint($antenne['lon'],$antenne['lat']);
                $distance = distance($coords,$coords_antenne);
                $new_tableau_associatif[$j]['dist'] = $distance;
                $j++;
            }
        }
        //tri par distance
        $array_distance = array_column($new_tableau_associatif, 'dist');
        array_multisort($array_distance, SORT_NUMERIC, $new_tableau_associatif);
        return array_slice($new_tableau_associatif,0,$N,true);
    }

    function to_JSON_antennes($coords,$N,$tableau_associatif) {
        echo json_encode(tab_N_dist_antennes($coords,$N,$tableau_associatif));
    }

    function to_HTML_antennes($coords,$N,$operateur,$tableau_associatif) {
        $tableau_associatif_N = tab_N_dist_antennes($coords,$N,$operateur,$tableau_associatif);
        $NbLigne = count($tableau_associatif_N);
        $Nbcol = count($tableau_associatif_N[0]);
        echo '<table>';
        echo '<thead><tr>';
        echo '<th>'."Opérateur".'</th>';
        echo '<th>'."Emplacement".'</th>';
        echo '<th>'."Support 4G?".'</th>';
        echo '<th>'."Longitude".'</th>';
        echo '<th>'."Latitude".'</th>';
        echo '<th>'."Distance (en m)".'</th>';
        echo '</tr></thead>';
        echo '<tbody>';
        // lignes suivantes
        for ($i=0; $i<$NbLigne; $i++) {
         echo '<tr>';
         foreach($tableau_associatif_N[$i] as $key) {
            echo '<td>';
            // AFFICHAGE ligne $i, colonne $key
            echo $key;
            echo '</td>';
          }
          echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
    }
?>