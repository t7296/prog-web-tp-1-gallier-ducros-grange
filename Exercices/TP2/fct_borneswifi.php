<?php
require_once("../../Helpers/tp2-helpers.php");

    //Renvoie un tableau de tableau associatif avec nom, adr, lat, lon
    function bornes_as_array($filename) {
        $array_tmp=file($filename);
        $tableau_associatif;
        for($i=0; $i<count($array_tmp); $i++) {
            $array=str_getcsv($array_tmp[$i]);

            #pour determiner la localisation (Q7)
            $adresse = "https://api-adresse.data.gouv.fr/reverse/?lon=".$array[2]."&lat=".$array[3];
            $output = smartcurl($adresse,0);
            $json_geocode = json_decode($output,true);
            if ($json_geocode['features']==NULL)
                $location = "Location Unknown";
            else
                $location = $json_geocode['features']['0']['properties']['label']."\n";
            ######

            $tableau_associatif[$i] = [
                'name' => $array[0],
                'adr' => $array[1],
                'lon' => $array[2],
                'lat' => $array[3],
                'location' => $location,
            ];
        }
        //print_r($tableau_associatif); decommentez pr afficher les valeurs du tableau
        return $tableau_associatif;
    }

    function tab_N_bornes_dist($coords,$N,$tableau_associatif) {
        $j=0;
        foreach($tableau_associatif as $borne ) {
            $coords_borne = geopoint($borne['lon'],$borne['lat']);
            $distance = distance($coords,$coords_borne);
            $tableau_bornes_distance[$j] = [ 'name' => $borne['name'] , 'dist' => $distance ];
            $j++;
        }
        $array_distance = array_column($tableau_bornes_distance, 'dist');
        array_multisort($array_distance, SORT_NUMERIC, $tableau_bornes_distance);
        for($j=0; $j<$N; $j++) {
            $name_borne = $tableau_bornes_distance[$j]['name'];
            $distance = $tableau_bornes_distance[$j]['dist'];
            echo "$name_borne , $distance";
            echo "<br>"; //mettre \n ds le echo precedent pr l'affichage en ln de comm et enlever cette ln
        }
    }

    function tab_N_dist($coords,$N,$tableau_associatif) {
        $j=0;
        foreach($tableau_associatif as $borne ) {
            $coords_borne = geopoint($borne['lon'],$borne['lat']);
            $distance = distance($coords,$coords_borne);
            $tableau_associatif[$j]['dist'] = $distance;
            $j++;
        }
        $array_distance = array_column($tableau_associatif, 'dist');
        array_multisort($array_distance, SORT_NUMERIC, $tableau_associatif);
        return array_slice($tableau_associatif,0,$N,true);
    }
    function to_JSON($coords,$N,$tableau_associatif) {
        echo json_encode(tab_N_dist($coords,$N,$tableau_associatif));
    }

    function to_HTML($coords,$N,$tableau_associatif) {
        $tableau_associatif_N = tab_N_dist($coords,$N,$tableau_associatif);
        $NbLigne = count($tableau_associatif_N);
        $Nbcol = count($tableau_associatif_N[0]);
        echo '<table>';
        echo '<thead><tr>';
        echo '<th>'."Nom borne".'</th>';
        echo '<th>'."Emplacement".'</th>';
        echo '<th>'."Longitude".'</th>';
        echo '<th>'."Latitude".'</th>';
        echo '<th>'."Adresse".'</th>';
        echo '<th>'."Distance (en m)".'</th>';
        echo '</tr></thead>';
        echo '<tbody>';
        // lignes suivantes
        for ($i=0; $i<$NbLigne; $i++) {
         echo '<tr>';
         foreach($tableau_associatif_N[$i] as $key) {
            echo '<td>';
            // AFFICHAGE ligne $i, colonne $key
            echo $key;
            echo '</td>';
          }
          echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
    }
?>