% PW-DOM  Compte rendu de TP

# Compte-rendu du TP1 - Exercices PHP


## Participants 

* Mathis Grange
* Loric Gallier
* Vincent Ducros

## Prise en main - diagnostic PHP (niv. 1)

Ctrl+U dans un navigateur pour avoir l'affichage source (similaire a ce qu'on obtient avec la ligne de commandes)

Quand on introduit une erreur :
PHP *Parse error:*  syntax error, unexpected ',' in *file* on line *xx* 

## Calcul d’intérêts composés (niv. 1-2) 

Voir [calcul.html](./calcul.html), [resultat.php](./resultat.php), [calcul.php](./calcul.php) et [libcalcul.php](./libcalcul.php)
GET pour partager avec quelqu'un le resultat du formulaire, modifier sans repasser par le formulaire
POST pour données sensibles, 

## Un peu de style en CSS (niv. 1)

Les éléments du tableau ln 41-43 , 48-50 , 53-55 , 58-60 , 63-65 du fichier source sont placés à une profondeur de 5 sous :
html > body > table > thead / tbody > tr > th / td

Voir [elements.html](./elements.html)

## Table de multiplication (niv. 1-2)

Voir [multiplication.php](./multiplication.php)



