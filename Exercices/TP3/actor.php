<?php require_once("fct_tmdb_q7and9.php");?>
<!DOCTYPE HTML>

<html lang="fr">
    <head>
        <link rel="stylesheet" type="text/css" href="formulaire.css" />
        <title>Actor Roles Finder</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </head>

    <nav>
        <a href="tmdb.php">Movie finder</a>
        <a href="q6tmdb.php">LOTR collection</a>
        <a href="q7tmdb_actors.php">Actor from LOTR</a>
        <a href="actor.php">Roles finder</a>
    </nav>

    <body>
        <div class="formulaire">
            <p> Enter an id to get basic actor infos</p>
            <form method="get" action="actor.php">
                <ul>
                    <label for="Id">id</label> 
                    <input type="number" step="any" id="id" name="id" value="<?php echo $_GET['id'] ?>"/> <br /> <br />
                </ul> <br />
                <input type="submit" value="Find roles" />
            </form>
        </div>
        <?php if (isset($_GET['id'] ) ) {
            echo '<div class="table">';
            echo "<a href=\"https://www.themoviedb.org/person/".$_GET['id']."\">More info over on TMDB</a></td>";
            to_HTML_roles(get_roles_actor($_GET['id']));
            echo '</div>';
        }
     ?>
    </body>
</html>