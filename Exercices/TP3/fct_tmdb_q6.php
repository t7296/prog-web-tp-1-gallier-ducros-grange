<?php
require_once("../../Helpers/tp3-helpers.php");

    function get_lotr_collection() {
        $collection = "collection/119";
        $param = [ null , ['language' => 'en'], ['language' => 'fr']];
        // on initialise les tableaux ou chaque case = 1 film et chaque sous-case = 1 version 
        $title = [[], [], []];
        $original_title = [[], [], []];
        $overview = [[], [], []];
        $release_date = [[], [], []];
        $homepage = [[], [], []];
        $poster_path = [[], [], []];
        //recuperation des infos pour la VO, la VF et la VA avec des requetes
        for($i=0;$i<3;$i++) {
            $output = tmdbget($collection, $param[$i]);
            $array_tmp=json_decode($output,true);
            $j=0;
            foreach($array_tmp['parts'] as $movie) { //on recupere chaque film 1 a 1 dans une version donnee

                array_push($title[$j], $movie['title']);
                array_push($original_title[$j], $movie['original_title']);
                array_push($overview[$j], $movie['overview']);
                array_push($release_date[$j], $movie['release_date']);
                //creation du lien vers la page dans TMDB
                $homepage_path = "https://www.themoviedb.org/movie/".$movie['id']."-".str_replace(" ", "-", $movie['original_title']);
                array_push($homepage[$j], $homepage_path);
                array_push($poster_path[$j], $movie['poster_path']);
                $j++;
            }
        }
        //creation d'un tableau associatif avec toutes les infos necessaires $j = numero du film
        for($j=0;$j<3;$j++) {
            $movie_essentials[$j] = [
                'title' => $title[$j],
                'original_title' => $original_title[$j],  
                'overview' => $overview[$j],
                'release_date' => $release_date[$j],
                'homepage' => $homepage[$j],
                'poster' => $poster_path[$j],
            ];
        }
        return $movie_essentials;
    }

    function to_html($movie_essentials) {
        echo '<table>';
        echo '<thead><tr>';
        echo '<th>'."".'</th>';
        echo '<th>'."Original version".'</th>';
        echo '<th>'."English version".'</th>';
        echo '<th>'."French version".'</th>';
        echo '</tr></thead>';
        echo '<tbody>';
        foreach($movie_essentials as $movie_essentials_movie_nb_x) {
            echo '<tr><td colspan="4">'.$movie_essentials_movie_nb_x['original_title'][0].'</td></tr>';
            foreach($movie_essentials_movie_nb_x as $key => $value) {
                echo '<tr>';
                if ($key == 'poster') {
                    echo '<th>Poster</th>';
                    echo '<td><img src="https://image.tmdb.org/t/p/w500'.$value[0].'"></td>';
                    echo '<td><img src="https://image.tmdb.org/t/p/w500'.$value[1].'"></td>';
                    echo '<td><img src="https://image.tmdb.org/t/p/w500'.$value[2].'"></td>';
                } else if ($key == 'homepage') {
                    echo '<th>'.$key.'</th>';
                    echo '<td>'."<a href=".$value[0].">".$value[0]."</a>".'</td>';
                    echo '<td>'."<a href=".$value[1].">".$value[1]."</a>".'</td>';
                    echo '<td>'."<a href=".$value[2].">".$value[2]."</a>".'</td>';
                    echo '</tr>';                    
                } else {
                    echo '<th>'.$key.'</th>';
                    echo '<td>'.$value[0].'</td>';
                    echo '<td>'.$value[1].'</td>';
                    echo '<td>'.$value[2].'</td>';
                    echo '</tr>';
                }
            }
        }
        echo '</tbody>';
        echo '</table>';
    }

?>