<?php require_once("fct_tmdb.php");?>
<!DOCTYPE HTML>

<html lang="fr">
    <head>
        <link rel="stylesheet" type="text/css" href="formulaire.css" />
        <title>Movie Finder</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </head>

    <nav>
        <a href="tmdb.php">Movie finder</a>
        <a href="q6tmdb.php">LOTR collection</a>
        <a href="q7tmdb_actors.php">Actor from LOTR</a>
        <a href="actor.php">Roles finder</a>
    </nav>

    <body>
        <div class="formulaire">
            <p> Enter an id to get basic movie infos</p>
            <form method="get" action="tmdb.php">
                <ul>
                    <label for="Id">id</label> 
                    <input type="number" step="any" id="id" name="id" value="<?php echo $_GET['id'] ?>"/> <br /> <br />
                </ul> <br />
                <input type="submit" value="Find movie" />
            </form>
        </div>
        <?php if (isset($_GET['id'] ) ) {
            echo '<div class="table">';
            to_HTML(get_movie($_GET['id']));
            echo '</div>';
        }
     ?>
    </body>
</html>