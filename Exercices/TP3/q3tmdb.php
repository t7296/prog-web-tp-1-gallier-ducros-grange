<?php require_once("fct_tmdb_q3.php");?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="formulaire.css" />
        <title>Movie Finder</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </head>
    <body>
        <div class="formulaire">
            <p> Enter an id to get basic movie infos</p>
            <form method="get" action="q3tmdb.php">
                <ul>
                    <label for="Id">id</label> 
                    <input type="number" step="any" id="id" name="id" value="<?php echo $_GET['id'] ?>"/> <br /> <br />
                </ul> <br />
                <input type="submit" value="Find movie" />
            </form>
        </div>
        <?php if (isset($_GET['id'] ) ) {
            echo '<div class="table">';
            to_HTML(get_movie($_GET['id']));
            echo '</div>';
        }
     ?>
    </body>
</html>