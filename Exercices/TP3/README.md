% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

## Participants 

* Gallier Loric
* Ducros Vincent
* Grange Mathis

## Introduction

Le but de ce TP est de manier des requêtes API renvoyant des .json et des fluxs rss en créant des fonctions pour afficher dans une page HTML les informations récupérées.

## Utilisation de The Movie Database

### Question 1 - Exploration

En lançant la requête API avec le film 550 de la base de données, on récuprère un fichier .json.
Le film en question est *Fight Club* sorti le 15 octobre 1999 aux Etats-Unis.
En précisant dans la requête le paramètre `language=fr`, *overview* et *tagline* sont la version française du résumé et de la phrase d'accroche du film, la date de sortie reste la même alors que le film est sorti le 10 novembre 1999 en France.

### Question 2 - Exploration CLI

Avec `curl -L http://api.themoviedb.org/3/movie/500?api_key=ebb02613ce5a2ae58fde00f4db95a9c1` on récupère sur la sortie standard la réponse au format json.
On peut lancer q2tmdb.php avec `php q2tmdb.php` qui transforme le json obtenu en un tableau associatif php.

### Question 3 - Page de détail (Web) 

Voir [q3tmdb.php](./q3tmbdb.php) et [fct_tmdb_q3.php](./fct_tmdb_q3.php).

On a une fonction get_movie qui envoie la requête et met dans un tableau les clés qui nous intéressent et une fonction to_HTML qui génère une table. Le tout est utilisé dans *q3tmdb.php* qui affiche le tableau proprement. 

### Question 4 - Page en 3 colonnes 

Voir [tmbd.php](./tmbdb.php) et [fct_tmdb.php](./fct_tmdb.php).

On a modifié la fonction get_movie pour envoyer 3 requêtes (une par langue), on remplit dans des tables séparés les titres, les description etc... . Cela permet d'afficher la ligne des titres d'un seul coup dans la fonction to_HTML.

### Question 5 - Image (web)

Voir [tmdb.php](./tmbdb.php) et [fct_tmdb.php](./fct_tmdb.php).

On récupère la clé `poster_path` dans le json qu'on ajoute à la fin de l'URL `https://image.tmdb.org/t/p/w500`.

### Question 6 - Requête (web)

Voir [q6tmdb.php](./q6tmbdb.php) et [fct_tmdb_q6.php](./fct_tmdb_q6.php).

On fait une requete avec `http://api.themoviedb.org/3/collection/119?api_key=ebb02613ce5a2ae58fde00f4db95a9c1` pour obtenir les informations sur la trilogie du Seigneur des Anneaux.
On intègre les informations dans un tableau associatif avec une entrée pour chaque film. Chaque champ (ex : titre, overview) a trois entrée (pour la VO, la VA et la VF). 
On fait l'affichage film par film et chaque film est géré comme dans les questions précédentes.

### Question 7 - Distribution (web)

Voir [q7tmdb.php](./q7tmbdb_actors.php) et [fct_tmdb_q7and9.php](./fct_tmdb_q7and9.php).

On fait une requête API pour chacun des 3 films avec `/credits`. On construit au fur et à mesure un tableau de tableau associatif avec la personne (nom pour le tableau) et les informations (rôles, nombre d'apparences, id de l'acteur).
Pour l'affichage, on reprend basiquement ce qu'on a fait précédemment et on affiche les infos des différents acteurs du Seigneur des Anneaux.

### Question 8 - Distribution ++

Il n'y a pas de manière évidente de récupérer la liste de tous les acteurs qui jouent un Hobbit. En effet, une piste serait de faire une recherche dans le champ `character` avec le mot "Hobbit" (sans casse) (cela permettrait de récupérer une liste de 15 acteurs). Cependant, d'autres acteurs qui incarnent des Hobbits ne sont pas récupérables de cette façon car le mot "Hobbit" n'apparaît pas dans la description du personnage qu'ils incarnent (par exemple Elijah Wood qui joue Frodon ne serait pas dans la liste).

### Question 9 - Casting

Voir [q7tmdb.php](./q7tmbdb_actors.php) et [fct_tmdb_q7and9.php](./fct_tmdb_q7and9.php) ainsi que [actor.php](./actor.php) 

On a rajouté la fonctionnalité depuis la page de q7tmdb de pouvoir cliquer sur un nom d'acteur, ce qui nous renvoie vers une nouvelle page php avec l'id de l'acteur passé par la méthode GET. On peut également remplir un formulaire avec un id d'acteur.
Cette page renvoie la liste des films dans lequel l'acteur choisit à jouer ainsi que les rôles/travaux réalisés.

Un défaut est que sur la page de l'acteur, on ne peut avoir son nom car celui-ci n'est pas donné dans la requête API avec l'id qu'on passe en paramètre.

### Question 10 - Bande-annonce

Voir [tmdb.php](./tmbdb.php) et [fct_tmdb.php](./fct_tmdb.php). (les mêmes pages que la question 4 et 5)

On a intégré directement à la recherche de films par id des lecteurs YouTube embedded avec des balises `<iframe>`.
On a pu faire ça après avoir récupérer par une requête la liste des clés des vidéos youtube associé à l'id du film qu'on cherche. 
On aurait pu faire le choix de n'afficher que la 1ère vidéo pour chaque version mais on a décidé d'afficher toute la liste à chaque fois, ce qui peut générer des pages avec beaucoup de vidéos.


## Analyse d'un flux RSS de podcast

On traite dans cette partie uniquement la partie mise en jambes. 

### Question 0 - Dépendances

Voir [test_biblio.php](./test_biblio.php) (à exécuter en ligne de commandes).

Après l'installation de la librairie `rss-php` avec `Composer` on a 2 fichiers `composer.json` et `composer.lock` ainsi qu'un répertoire `vendor` avec à l'intérieur, 1 répertoire pour composer et 1 autre pour la surcouche du parser XML qu'on vient d'installer.

Lorsqu'on lance test_biblio, on parse le flux RSS reçu et on stocke ça dans un objet, on peut ensuite récupérer les informations de base telle que le titre de la série de podcast et sa description. Ensuite, on a la liste de tous les podcasts de la série avec titre et description.

### Question 1 - Tableau des podcasts

Voir [podcast.php](./podcast.php).

Cette fois on a géré le php sur la même page que l'affichage html (moins modulaire).
On récupère les informations comme dans la question précédente et on affiche une table où chaque ligne correspond à un podcast. On a un lien vers la page de France Culture et un lien pour télécharger le podcast au format mp3. On a également un lecteur audio pour écouter directement depuis cette page le podcast.
Grâce au flux RSS, cette page est maintenue à jour automatiquement et affichera les derniers podcasts sortis.

## Conclusion

Dans ce TP, on a appris à gérer l'affichage dans des tableaux de différentes informations récupérés par des requêtes API. On a dans un premier temps utilisé la base de données de IMDB pour récupérer des informations sur des films et acteurs puis on a implémenté une page basique pour afficher les podcasts d'une série.