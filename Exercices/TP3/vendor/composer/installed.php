<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '8708796534bd2ad72e7ad08a63465db3d72716dc',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '8708796534bd2ad72e7ad08a63465db3d72716dc',
            'dev_requirement' => false,
        ),
        'dg/rss-php' => array(
            'pretty_version' => 'v1.5',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dg/rss-php',
            'aliases' => array(),
            'reference' => '18f00ab1828948a8cfe107729ca1f11c20129b47',
            'dev_requirement' => false,
        ),
    ),
);
