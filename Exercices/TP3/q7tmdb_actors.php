<?php require_once("fct_tmdb_q7and9.php");?>
<!DOCTYPE HTML>

<html lang="fr">
    <head>
        <link rel="stylesheet" type="text/css" href="formulaire.css" />
        <title>Actors : LOTR</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </head>

    <nav>
        <a href="tmdb.php">Movie finder</a>
        <a href="q6tmdb.php">LOTR collection</a>
        <a href="q7tmdb_actors.php">Actor from LOTR</a>
        <a href="actor.php">Roles finder</a>
    </nav>

    <body>
        <?php 
            echo '<div class="table">';
            to_HTML_actors(get_lotr_actors());
            echo '</div>';
     ?>
    </body>
</html>