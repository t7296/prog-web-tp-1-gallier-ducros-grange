<?php
	require_once("vendor/dg/rss-php/src/Feed.php");

	$url = "http://radiofrance-podcast.net/podcast09/rss_14312.xml";

	$rss = Feed::loadRss($url);
?>
<!DOCTYPE HTML>

<html lang="fr">
    <head>
        <link rel="stylesheet" type="text/css" href="formulaire.css" />
        <title>Podcast</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </head>

    <body>
    <div class="table">
        <?php
        echo '<h2>'. $rss->title .'</h2></br>';
        echo '<h3>'. $rss->description .'</h3></br>';
        ?>
        <table>
			<tr>
				<th>Date</th>
				<th>Titre (avec lien)</th>
				<th>Lecture</th>
				<th>Durée</th>
				<th>Télécharger</th>
			</tr>

			<?php
				foreach ($rss->item as $item) {
					echo '<tr>';
					echo '<td>' . $item->{'pubDate'} . '</td>';
                    //title pour la tooltip avec la description
					echo '<td><a href="'. $item->{'link'} .'" title="'. $item->{'description'} .'">'. $item->{'title'} .'</a></td>';
					//creation lecteur audio
                    echo "<td><audio controls><source src=\"" . $item->{'enclosure'}["url"] . "\" type=\"audio/mpeg\"></audio></td>\n";
					echo '<td>'. $item->{'itunes:duration'} .'</td>';
					echo '<td><a href="'. $item->{'enclosure'}["url"] .'">Téléchargement</a></td>';
					echo "</tr>\n";
				}
			?>
		</table>
    </div>
    </body>
</html>