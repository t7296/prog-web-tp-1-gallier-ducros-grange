<?php
require_once("../../Helpers/tp3-helpers.php");

    function get_movie($id) {
        $movie = "movie/".$id;
        $output = tmdbget($movie);
        $array_tmp=json_decode($output,true);
        $movie_essentials = [
                'title' => $array_tmp['title'],
                'original_title' => $array_tmp['original_title'],
                'tagline' => $array_tmp['tagline'],       
                'overview' => $array_tmp['overview'],
                'release_date' => $array_tmp['release_date'],
                'homepage' => $array_tmp['homepage'],
        ];
    
        return $movie_essentials;
    }

    function to_html($movie_essentials) {
        $NbLigne = count($movie_essentials);
        $Nbcol = 1;
        echo '<table>';
        echo '<thead><tr>';
        echo '<th>'."".'</th>';
        echo '<th>'."Original version".'</th>';
        echo '</tr></thead>';
        echo '<tbody>';
        foreach($movie_essentials as $key => $value) {
            echo '<tr>';
            echo '<th>'.$key.'</th>';
            echo '<td>';
            // AFFICHAGE ligne $i, colonne $key
            echo $value;
            echo '</td>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
    }

?>