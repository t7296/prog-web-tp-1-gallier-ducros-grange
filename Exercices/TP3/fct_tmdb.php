<?php
require_once("../../Helpers/tp3-helpers.php");

    function get_movie($id) {
        $movie = "movie/".$id;
        $param = [ ['append_to_response' => 'videos'] , 
                   ['language' => 'en' ,'append_to_response' => 'videos'], 
                   ['language' => 'fr', 'append_to_response' => 'videos'] ];
        // on initialise les tableaux ou chaque case = 1 version 
        $title = [];
        $original_title = [];
        $tagline = [];
        $overview = [];
        $release_date = [];
        $homepage = [];
        $poster_path = [];
        $videos = [[] , [] , []]; // 1 tableau de trailers pour chaque version
        //recuperation des infos pour la VO, la VF et la VA avec des requetes
        for($i=0;$i<3;$i++) {
            $output = tmdbget($movie, $param[$i]);
            $array_tmp=json_decode($output,true);
            array_push($title, $array_tmp['title']);
            array_push($original_title, $array_tmp['original_title']);
            array_push($tagline, $array_tmp['tagline']);
            array_push($overview, $array_tmp['overview']);
            array_push($release_date, $array_tmp['release_date']);
            //creation du lien vers la page dans TMDB
            $homepage_path = "https://www.themoviedb.org/movie/".$id."-".str_replace(" ", "-", $array_tmp['original_title']);
            array_push($homepage, $homepage_path);
            array_push($poster_path, $array_tmp['poster_path']);
            //on recupere les cles youtube des trailers si il y en a
            foreach($array_tmp['videos']['results'] as $trailers) {
                array_push($videos[$i], $trailers['key']);
            }
        }
        //creation d'un tableau associatif avec toutes les infos necessaires
        $movie_essentials = [
                'title' => $title,
                'original_title' => $original_title,
                'tagline' => $tagline,       
                'overview' => $overview,
                'release_date' => $release_date,
                'homepage' => $homepage,
                'poster' => $poster_path,
                'videos' => $videos,
        ];
        return $movie_essentials;
    }

    //fonction pour l'affichage des informations ds un tableau 3 colonnes en html
    function to_html($movie_essentials) {
        echo '<table>';
        echo '<thead><tr>';
        echo '<th>'."".'</th>';
        echo '<th>'."Original version".'</th>';
        echo '<th>'."English version".'</th>';
        echo '<th>'."French version".'</th>';
        echo '</tr></thead>';
        echo '<tbody>';
            foreach($movie_essentials as $key => $value) {
                echo '<tr>';
                if ($key == 'poster') {
                    $url_img = "https://image.tmdb.org/t/p/w500";
                    echo '<th>Poster</th>';
                    echo '<td><img src="'.$url_img.$value[0].'"></td>';
                    echo '<td><img src="'.$url_img.$value[1].'"></td>';
                    echo '<td><img src="'.$url_img.$value[2].'"></td>';
                } else if ($key == 'homepage') {
                    echo '<th>'.$key.'</th>';
                    echo '<td>'."<a href=".$value[0].">".$value[0]."</a>".'</td>';
                    echo '<td>'."<a href=".$value[1].">".$value[1]."</a>".'</td>';
                    echo '<td>'."<a href=".$value[2].">".$value[2]."</a>".'</td>';
                    echo '</tr>';       
                } else if ($key == 'videos') {
                    echo '<th>Trailers</th>';
                    foreach($value as $trailers_version_x) { // on a une boucle pour séparer VO / VA / VF
                        echo "<td>";
                        foreach ($trailers_version_x as $yt_key) { //puis on affiche chaque trailer de la version 1 par 1
                                echo '<iframe src="https://www.youtube.com/embed/'.$yt_key.'" height="281" width="500" allowfullscreen=""></iframe>';
                        }
                        echo "</td>";
                    }                  
                } else {
                    echo '<th>'.$key.'</th>';
                    echo '<td>'.$value[0].'</td>';
                    echo '<td>'.$value[1].'</td>';
                    echo '<td>'.$value[2].'</td>';
                    echo '</tr>';
                }
            }
        echo '</tbody>';
        echo '</table>';
    }

?>