<?php //Fichier de test simple de la librairie rss-php
require_once('vendor/dg/rss-php/src/Feed.php');

$url = "http://radiofrance-podcast.net/podcast09/rss_14312.xml";

$rss = Feed::loadRss($url);

echo 'Title: ', $rss->title ."\n";
echo 'Description: ', $rss->description ."\n";
//echo 'Link: ', $rss->url ."\n";
echo "========================\n";
foreach ($rss->item as $item) {
	echo 'Title: ', $item->title ."\n";
	//echo 'Link: ', $item->url ."\n";
	echo 'Timestamp: ', $item->timestamp ."\n";
	echo 'Description ', $item->description ."\n";
	//echo 'HTML encoded content: ', $item->{'content:encoded'} ."\n";
    echo "========================\n";
}
?>